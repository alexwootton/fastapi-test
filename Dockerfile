FROM tiangolo/uvicorn-gunicorn:python3.7

COPY requirements.txt .

RUN pip install --upgrade pip && pip install -r requirements.txt

COPY ./app /app